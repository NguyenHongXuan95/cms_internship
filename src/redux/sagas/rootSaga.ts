import { all } from "@redux-saga/core/effects"
import product from "../sagas/product"

export function* rootSagas() {
    yield all(
       [ product()]
    )
}