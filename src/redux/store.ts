import createSagaMiddleware from "@redux-saga/core";
import { configureStore } from "@reduxjs/toolkit";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import reducers from "./reducers";
import { rootSagas } from "./sagas/rootSaga";

const middleware: any[] = [];

const persistConfig = {
  key: "root",
  storage,
  blacklist: ["blacklist"],
};
const sagaMiddleware = createSagaMiddleware();
middleware.push(sagaMiddleware);


const persistedReducer = persistReducer(persistConfig, reducers);
const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }).concat(middleware),
});

sagaMiddleware.run(rootSagas);
export type RootState = ReturnType<typeof store.getState>;
export const persistor = persistStore(store);

export default store;
