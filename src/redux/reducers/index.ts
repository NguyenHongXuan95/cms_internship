import { combineReducers } from "@reduxjs/toolkit";
import product from "../reducers/product";


const rootReducer = combineReducers({
 product,
});
export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
