import React, { useEffect } from "react";
import "./App.css";
import CustomContainner from "./Container/CustomContainner";
import FormValidation from "./Component/Form/FormValidation";
import { useSelector } from "react-redux";
import { selectorComponent } from "./redux/reducers/product";
import FormWizard from "./Component/Form/FormWizard";

function App() {
  const renderComponent = useSelector(selectorComponent);

  switch (renderComponent) {
    case "Form Wizard":
      return <FormWizard />;
    case "Form Validation":
      return <FormValidation />;
    default:
      <FormValidation />;
      break;
  }

  return <FormValidation />;
}

export default App;
