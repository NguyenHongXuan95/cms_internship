import React from "react";
import * as yup from "yup";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import {
  Box,
  Button,
  InputAdornment,
  TextField,
  Typography,
  styled,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import FormCustom from "../../Container/FormCustom";
import { Checkbox } from "@mui/joy";
import CustomContainner from "../../Container/CustomContainner";

interface IFormInputs {
  firstname: string;
  lastname: string;
  city: string;
  state: string;
  zip: string;
}
declare module "yup" {
  interface StringSchema {
    firstNameValidate(firstname: string): StringSchema;
    lastNameValidate(lastname: string): StringSchema;
    cityValidate(city: string): StringSchema;
    stateValidate(state: string): StringSchema;
    zipValidate(zip: string): StringSchema;
  }
}

export default function FormValidation() {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.down("md"));

  const linkbreadcrumb = ["Form", "Form Validation"];

  const firstNameyup = yup
    .string()
    .min(6, "Tên quá ngắn!")
    .required("Làm ơn nhập tên hộ cái");
  const lastNameyup = yup
    .string()
    .min(6, "Tên quá ngắn!")
    .required("Làm ơn đừng quên nhập họ và tên đệm");
  const cityyup = yup.string().required("Làm ơn nhập địa chỉ thành phố");
  const stateyup = yup.string().required("Làm ơn nhập tên tỉnh");
  const zipyup = yup.string().required("Làm ơn mã zip");

  const FormScheama = yup.object().shape({
    firstname: firstNameyup,
    lastname: lastNameyup,
    city: cityyup,
    state: stateyup,
    zip: zipyup,
  });

  const handleSubmit = (
    firstname: string,
    lastname: string,
    city: string,
    state: string,
    zip: string
  ) => {};

  const onSubmit: SubmitHandler<IFormInputs> = (data) => {
    handleSubmit(
      data.firstname,
      data.lastname,
      data.city,
      data.state,
      data.zip
    );
  };

  const {
    control,
    handleSubmit: handleReactHookFormSubmit,
    formState: { errors },
  } = useForm<IFormInputs>({
    resolver: yupResolver(FormScheama),
  });

  return (
    <CustomContainner title="Form Validation" breadcrumb={linkbreadcrumb}>
      <Wrapper p={2}>
        <form onSubmit={handleReactHookFormSubmit(onSubmit)}>
          <Typography
            fontSize={"15px"}
            fontWeight={500}
            color={"#eff2f7"}
            fontFamily={"inherit"}
            mb={4}
          >
            Form Validation
          </Typography>
          <Box
            display={!isMd ? "flex" : "block"}
            justifyContent={"space-between"}
          >
            <FormCustom
              name={"firstname"}
              control={control}
              error={errors.firstname}
              placehoder="First name"
              widthstyle="48%"
            />
            <FormCustom
              name={"lastname"}
              control={control}
              error={errors.lastname}
              placehoder="First name"
              widthstyle="48%"
            />
          </Box>
          <Box
            display={!isMd ? "flex" : "block"}
            justifyContent={"space-between"}
            mt={4}
          >
            <FormCustom
              name={"city"}
              control={control}
              error={errors.city}
              placehoder="City"
              widthstyle="32%"
            />
            <FormCustom
              name={"state"}
              control={control}
              error={errors.state}
              placehoder="State"
              widthstyle="32%"
            />
            <FormCustom
              name={"zip"}
              control={control}
              error={errors.zip}
              placehoder="Zip"
              widthstyle="32%"
            />
          </Box>{" "}
          <Checkbox
            label="Tôi đồng ý các thông trên là đúng"
            sx={{ color: "#8590a5", m: "24px 0" }}
          />
          <Box className="Router-button">
            <Button
              type="submit"
              sx={{ textTransform: "none", color: "white", bgcolor: "#0bb197" }}
              variant="contained"
            >
              Submit form
            </Button>
          </Box>
        </form>

        <Box sx={{ height: "150px" }}></Box>
      </Wrapper>
    </CustomContainner>
  );
}

const Wrapper = styled(Box)`
  background-color: #2b3244;
  input {
    color: #8590a5;
  }
  .field {
    border: 0.25px solid #8590a5;
    border-radius: 5px;
  }
`;
