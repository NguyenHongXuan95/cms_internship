import React, { useState } from "react";
import CustomContainner from "../../Container/CustomContainner";
import { StepIconProps } from "@mui/material/StepIcon";
import LiveHelpRoundedIcon from "@mui/icons-material/LiveHelpRounded";
import WalletRoundedIcon from "@mui/icons-material/WalletRounded";
import AssignmentTurnedInRoundedIcon from "@mui/icons-material/AssignmentTurnedInRounded";
import {
  Box,
  Button,
  Step,
  StepLabel,
  Stepper,
  Typography,
  styled,
  useMediaQuery,
  useTheme,
} from "@mui/material";

export default function FormWizard() {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.down("md"));

  const linkbreadcrumb = ["Form", "Form Wizard"];
  const steps = ["Seller Details", "Company Document", "Bank Details"];
  const [step, setStep] = useState<number>(-1);
  const ColorlibStepIconRoot = styled("div")<{
    ownerState: { completed?: boolean; active?: boolean };
  }>(({ theme, ownerState }) => ({
    backgroundColor:
      theme.palette.mode === "dark" ? theme.palette.grey[700] : "#ccc",
    zIndex: 1,
    color: "#fff",
    width: 50,
    height: 50,
    display: "flex",
    borderRadius: "50%",
    justifyContent: "center",
    alignItems: "center",
    padding: "0 0 0 0",
    ...(ownerState.active && {
      backgroundImage:
        "linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)",
      boxShadow: "0 4px 10px 0 rgba(0,0,0,.25)",
    }),
    ...(ownerState.completed && {
      backgroundImage:
        "linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)",
    }),
  }));

  function ColorlibStepIcon(props: StepIconProps) {
    const { active, completed, className } = props;

    const icons: { [index: string]: React.ReactElement } = {
      1: <LiveHelpRoundedIcon />,
      2: <WalletRoundedIcon />,
      3: <AssignmentTurnedInRoundedIcon />,
    };

    return (
      <ColorlibStepIconRoot
        ownerState={{ completed, active }}
        className={className}
      >
        {icons[String(props.icon)]}
      </ColorlibStepIconRoot>
    );
  }

  return (
    <CustomContainner title="Form Wizard" breadcrumb={linkbreadcrumb}>
      <Wrapper p={2}>
        <Typography
          fontSize={"15px"}
          fontWeight={500}
          color={"#eff2f7"}
          fontFamily={"inherit"}
          mb={4}
        >
          Thông tin thanh toán
        </Typography>
        <Stepper activeStep={step} sx={{ px: 4 }}>
          {steps.map((item: any, index) => (
            <Step key={index}>
              <StepLabel
                StepIconComponent={ColorlibStepIcon}
                className="topIcon"
              >
                {!isMd && <Typography position={"absolute"}>{item}</Typography>}
              </StepLabel>
            </Step>
          ))}
        </Stepper>
        <Box mt={4}>
          <Button
            onClick={() => {
              if (step === -1) {
                setStep(-1);
              } else {
                setStep((prev) => (prev -= 1));
              }
            }}
          >
            back
          </Button>
          <Button
            onClick={() => {
              if (step >= 2) {
                setStep(2);
              } else {
                setStep((prev) => (prev += 1));
              }
            }}
          >
            next
          </Button>
        </Box>
      </Wrapper>
    </CustomContainner>
  );
}

const Wrapper = styled(Box)`
  background-color: #2b3244;
  p {
    color: #919bae;
  }
  .topIcon {
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    positon: relative;
  }
  .MuiStepLabel-iconContainer {
    padding-right: 0;
  }
  .MuiStepLabel-label {
    display: flex;
    justify-content: center;
  }
`;
