import {
  Avatar,
  Box,
  Divider,
  Grid,
  IconButton,
  InputAdornment,
  ListItemIcon,
  Menu,
  MenuItem,
  TextField,
  Typography,
  styled,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import MenuOpenRoundedIcon from "@mui/icons-material/MenuOpenRounded";
import SearchRoundedIcon from "@mui/icons-material/SearchRounded";
import PersonRoundedIcon from "@mui/icons-material/PersonRounded";
import WalletRoundedIcon from "@mui/icons-material/WalletRounded";
import SettingsRoundedIcon from "@mui/icons-material/SettingsRounded";
import NotificationsRoundedIcon from "@mui/icons-material/NotificationsRounded";
import React, { useState } from "react";
import LeftSide from "./LeftSide";
import NavigateNextRoundedIcon from "@mui/icons-material/NavigateNextRounded";

interface Props {
  children: React.ReactNode;
  title: string;
  breadcrumb: string[];
}

export default function CustomContainner({
  children,
  title,
  breadcrumb,
}: Props) {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.down("md"));

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const [openleft, setOpenleft] = useState(false);

  return (
    <Wrapper>
      <Grid container spacing={0.05}>
        {!isMd && (
          <Grid item xs={12} md={2} lg={2}>
            <LeftSide />
          </Grid>
        )}
        <Grid item xs={12} md={10} lg={10}>
          <Box
            display={"flex"}
            justifyContent={"space-between"}
            px={!isMd ? 4 : 1}
            py={2}
            bgcolor={"#272d3e"}
          >
            <Box className="search">
              <Box
                display={"flex"}
                justifyContent={"center"}
                alignItems={"center"}
                mx={2}
                onClick={() => setOpenleft(!openleft)}
              >
                <MenuOpenRoundedIcon />
              </Box>
              {!isMd ? (
                <TextField
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <SearchRoundedIcon />
                      </InputAdornment>
                    ),
                  }}
                  placeholder="search..."
                  className="searchfield"
                />
              ) : (
                <Box
                  display={"flex"}
                  justifyContent={"center"}
                  alignItems={"center"}
                >
                  <SearchRoundedIcon />
                </Box>
              )}
            </Box>
            <Box display={"flex"}>
              <Box className="icon">
                <NotificationsRoundedIcon />
              </Box>{" "}
              <>
                <IconButton
                  onClick={handleClick}
                  size="small"
                  sx={{ ml: 2 }}
                  aria-controls={open ? "account-menu" : undefined}
                  aria-haspopup="true"
                  aria-expanded={open ? "true" : undefined}
                >
                  <Avatar />
                </IconButton>
                <Menu
                  anchorEl={anchorEl}
                  id="account-menu"
                  open={open}
                  onClose={handleClose}
                  onClick={handleClose}
                  PaperProps={{
                    elevation: 0,
                    sx: {
                      overflow: "visible",
                      filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
                      mt: 1.8,
                      mb: 2,
                      bgcolor: "white",
                      color: "#767272;",
                      textTransform: "capitalize",
                      "& .MuiAvatar-root": {
                        width: 32,
                        height: 32,
                        ml: -0.5,
                        mr: 1.5,
                      },
                      "&:before": {
                        content: '""',
                        display: "block",
                        position: "absolute",
                        top: 0,
                        right: 18,
                        width: 10,
                        height: 10,
                        bgcolor: "inherit",
                        transform: "translateY(-50%) rotate(45deg)",
                        zIndex: 0,
                      },
                    },
                  }}
                  transformOrigin={{ horizontal: "right", vertical: "top" }}
                  anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
                >
                  <MenuItem onClick={handleClose}>
                    <ListItemIcon>
                      <PersonRoundedIcon fontSize="small" />
                    </ListItemIcon>
                    Profile
                  </MenuItem>
                  <Divider />
                  <MenuItem onClick={handleClose}>
                    <ListItemIcon>
                      <WalletRoundedIcon fontSize="small" />
                    </ListItemIcon>
                    My Wallet
                  </MenuItem>
                  <Divider />
                  <MenuItem onClick={handleClose}>
                    <ListItemIcon>
                      <SettingsRoundedIcon fontSize="small" />
                    </ListItemIcon>
                    Setting
                  </MenuItem>
                </Menu>
              </>
            </Box>
            {isMd && openleft && (
              <Box className="oversize">
                <LeftSide />
              </Box>
            )}
          </Box>
          <Box p={"32px 16px"} bgcolor={"#1d222e"} minHeight={1184}>
            <Box
              display={!isMd ? "flex" : "block"}
              justifyContent={"space-between"}
              mb={2}
            >
              <Typography fontSize={"18px"} fontWeight={550} color={"#eff2f7"}>
                {title}
              </Typography>
              <Box display={"flex"}>
                {breadcrumb &&
                  breadcrumb?.map((item: string, index: number) => (
                    <Box key={index} className="breadcrumbIcon">
                      <Typography color={"#f6f6f6"} fontSize={"14px"}>
                        {item}
                      </Typography>
                      {index !== breadcrumb.length - 1 && (
                        <Box className="breadcrumbIcon">
                          <NavigateNextRoundedIcon />
                        </Box>
                      )}
                    </Box>
                  ))}
              </Box>
            </Box>
            {children}
          </Box>
        </Grid>
      </Grid>
    </Wrapper>
  );
}

const Wrapper = styled(Box)`
  color: #8590a5;
  .oversize {
    position: absolute;
    z-index: 9999;
    left: 0;
    top: 82px;
  }
  .searchfield {
    div {
      border-radius: 5px;
      background-color: #2b3244;
      margin: auto 0;
    }
  }
  .search {
    display: flex;
    input {
      padding: 8px 6px;
      color: white;
    }

    svg {
      width: 24px;
      height: 24px;
    }
  }
  .icon {
    justify-content: center;
    display: flex;
    align-items: center;
    svg {
      width: 24px;
      height: 24px;
    }
  }
  .breadcrumbIcon {
    display: flex;
    justify-content: center;
    align-items: center;
    svg {
      width: 21px;
      height: 21px;
    }
  }
`;
