import {
  Box,
  TextField,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import React from "react";
import { Controller } from "react-hook-form";

interface Props {
  control: any;
  name: string;
  error: any;
  placehoder: string;
  widthstyle: string;
}
export default function FormCustom(data: Props) {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.down("md"));
  console.log(data, "data");

  return (
    <Box sx={{ width: !isMd ? data?.widthstyle : "100%", mb: !isMd ? 1 : 2 }}>
      <Typography fontWeight={550} mb={1}>
        {data?.placehoder}
      </Typography>
      <Controller
        name={data?.name}
        control={data?.control}
        render={({ field }) => (
          <TextField
            {...field}
            type="text"
            id="outlined-start-adornment"
            placeholder={data?.placehoder}
            error={!!data?.error}
            size="small"
            className="field"
            fullWidth
          />
        )}
      />
      <Typography mt={1} color={"red"} fontStyle={"italic"} fontSize={"12px"}>
        {data?.error?.message}
      </Typography>
    </Box>
  );
}
