import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Button,
  Typography,
  styled,
} from "@mui/material";
import ExpandMoreRoundedIcon from "@mui/icons-material/ExpandMoreRounded";
import OtherHousesRoundedIcon from "@mui/icons-material/OtherHousesRounded";
import React from "react";
import { useDispatch } from "react-redux";
import { getComponentRender } from "../redux/reducers/product";

interface Props {
  description: {
    name: string;
    link: string;
  }[];
  title: string;
}
interface Description {
  name: string;
  link: string;
}
export default function DropdownItem(data: Props) {
  const dispatch = useDispatch();
  return (
    <Wrapper>
      {" "}
      <Accordion sx={{ bgcolor: "inherit" }}>
        <AccordionSummary
          expandIcon={
            <ExpandMoreRoundedIcon
              fontSize="small"
              sx={{ m: "auto 0", color: "#8590a5" }}
            />
          }
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Button
            sx={{ p: "0 8px", fontSize: "15px" }}
            startIcon={
              <OtherHousesRoundedIcon
                sx={{ margin: "0 8px 4px 0", width: "18px", height: "22px" }}
              />
            }
          >
            {data?.title}
          </Button>
        </AccordionSummary>
        <AccordionDetails className="description">
          {data?.description?.map((item: Description, index: number) => (
            <Typography
              key={index}
              px={4}
              sx={{ cursor: "pointer" }}
              onClick={() => {
                dispatch(getComponentRender(item?.name));
              }}
            >
              {item?.name}
            </Typography>
          ))}
        </AccordionDetails>
      </Accordion>
    </Wrapper>
  );
}

const Wrapper = styled(Box)`
  .MuiAccordion-root {
    box-shadow: none;
  }
  .MuiAccordionSummary-root.Mui-expanded {
    min-height: 32px;
  }
  .MuiAccordionSummary-content.Mui-expanded {
    margin: 0px 0;
  }
  .description {
    color: #8590a5;
    p {
      margin: 10px 0;
    }
  }
  button {
    color: #8590a5;
    text-transform: none;
  }
`;
