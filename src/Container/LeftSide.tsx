import { Box, styled } from "@mui/material";
import React from "react";
import DropdownItem from "./DropdownItem";

export default function LeftSide() {
  const item = {
    title: "Form",
    description: [
      { link: "link1", name: "Form Wizard" },
      { link: "link2", name: "Form Validation" },
    ],
  };
  return (
    <Wrapper bgcolor={"#272d3e"} minHeight={1330}>
      <Box className="logo">
        <img
          src="/M.png"
          style={{ width: "30px", height: "30px", borderRadius: "50%" }}
          alt="logosize"
        />
        MeTa
      </Box>
      <Box>
        <DropdownItem {...item} />
      </Box>
    </Wrapper>
  );
}

const Wrapper = styled(Box)`
  .logo {
    display: flex;
    justify-content: center;
    align-items: center;
    min-height: 82px;
    font-size: 22px;
    img {
      margin-right: 8px;
    }
  }
`;
